# gst-test-plc

Simple app to test `stats` property on `opusdec` and `spanplc`.

Run by using either `cargo run` or `cargo run -- --spanplc`