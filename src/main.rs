use anyhow::Result;
use structopt::StructOpt;

use gst::prelude::*;
use gstreamer as gst;

use futures::prelude::*;

const PIPELINE_OPUS: &'static str = "audiotestsrc ! opusenc ! identity drop-probability=0.1 ! opusdec plc=true name=e ! autoaudiosink";
const PIPELINE_SPANPLC: &'static str =
    "audiotestsrc ! identity drop-probability=0.1 ! spanplc name=e ! autoaudiosink";

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short, long)]
    spanplc: bool,
}

async fn message_handler(loop_: glib::MainLoop, bus: gst::Bus) {
    let mut messages = bus.stream();

    while let Some(msg) = messages.next().await {
        use gst::MessageView;

        match msg.view() {
            MessageView::Eos(..) => loop_.quit(),
            MessageView::Error(err) => {
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.get_src().map(|s| s.get_path_string()),
                    err.get_error(),
                    err.get_debug()
                );
                loop_.quit();
            }
            _ => (),
        }
    }
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    let ctx = glib::MainContext::default();
    ctx.push_thread_default();
    let loop_ = glib::MainLoop::new(Some(&ctx), false);

    gst::init()?;

    let pipeline = if opt.spanplc {
        PIPELINE_SPANPLC
    } else {
        PIPELINE_OPUS
    };
    println!("{}", pipeline);
    let pipeline = gst::parse_launch(&pipeline)?;
    let bus = pipeline.get_bus().unwrap();

    pipeline.set_state(gst::State::Playing)?;

    let bin = pipeline.downcast_ref::<gst::Pipeline>().unwrap();
    let elt = bin.get_by_name("e").unwrap();
    let elt = elt.downgrade();

    glib::timeout_add_seconds(1, move || match elt.upgrade() {
        Some(elt) => {
            let stats = elt.get_property("stats").unwrap();
            let s = stats.get::<gst::Structure>().unwrap().unwrap();
            let num_samples = s.get::<u64>("plc-num-samples").unwrap().unwrap();
            let duration = s.get::<u64>("plc-duration").unwrap().unwrap();
            let duration = gst::ClockTime::from_nseconds(duration);

            dbg!(&s);
            println!("PLC generated {} samples ({})", num_samples, duration);
            glib::Continue(true)
        }
        None => glib::Continue(false),
    });

    ctx.spawn_local(message_handler(loop_.clone(), bus));

    loop_.run();

    pipeline.set_state(gst::State::Null)?;

    ctx.pop_thread_default();
    Ok(())
}
